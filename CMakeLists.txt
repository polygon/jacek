cmake_minimum_required(VERSION 3.8)

project(jacek-engine LANGUAGES CXX C)
# added language C becuse CMake yelled that CMAKE_C_COMPILE_OBJECT was not set

message("Looking for Vulkan package")
find_package(Vulkan REQUIRED)

message("Adding third_party subdirecotry")
add_subdirectory(third_party)

# Final executable will end up in the bin folder, with config subfolder
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/bin")

# Nice setting to use DX12
option(USE_DX12 "Use DirectX 12 instead of Vulkan (only for Windows)" ON)

option(SET_VK_EXTENSIONS_TO_13 "Change some Vk extensions' type names to Vk 1.3" OFF)

# Define sources which are only for Windows and DX12
if (WIN32 AND USE_DX12)
    set (WINDOWS_SOURCES
        src/d3dx12.h
        src/dx12_utils.h
        src/dx12_command_list.h
        src/dx12_command_list_allocator.h
        src/dx12_engine.cpp
        src/dx12_engine.h
        )
endif()

# Create new target and add source files to compile and link
message("Creating new target executable")
add_executable (jacek-engine
        ${WINDOWS_SOURCES}
        src/engine.cpp
        src/engine.h
        src/renderer.h
        src/main.cpp
        src/vk_engine.cpp
        src/vk_engine.h
        src/vk_types.h
        src/vk_initializers.cpp
        src/vk_initializers.h
        src/vulkan_memory_allocator_usage.cc
        src/ResourcesPaths.cpp
        src/ResourcesPaths.h
        
        third_party/vk-bootstrap/VkBootstrap.cpp
        third_party/volk/volk.c
        third_party/volk/volk.h
        third_party/VulkanMemoryAllocator/vk_mem_alloc.h

        src/Common/ArrayUtility.cpp
        src/Common/ArrayUtility.h
        src/Common/ArrayUtility.inl
        src/Common/Common.h
        
        src/Common/HelperMacros.h
        src/Common/Macro_VAARGS.h
        src/Common/MapUtility.h
        src/Common/NonCopyable.h
        src/Common/Singleton.h
        src/Common/StringUtility.h
        src/Common/StringUtility.cpp
        src/Common/TypeDefinitions.h
        src/Common/TypeIterator.h
        )

# We require C++ 20
target_compile_features(jacek-engine PRIVATE cxx_std_20)

# Pick rendering API
if (USE_DX12)
    target_compile_definitions(jacek-engine PRIVATE DX12=1)
endif ()

# Vulkan 1.3
if (SET_VK_EXTENSIONS_TO_13)
    target_compile_definitions(jacek-engine PRIVATE CHANGE_VK_EXTENSION_TYPENAMES_TO_13)
endif ()

# Set as startup project
set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT jacek-engine)

# SDL
message("Setting up SDL...")
if (WIN32)
    set(SDL2_DIR "${PROJECT_SOURCE_DIR}/third_party/SDL")
    set(SDL2_INCLUDE_DIR "${SDL2_DIR}/include")
    set(SDL2_LIBRARIES "${SDL2_DIR}/lib/x64/SDL2.lib;${SDL2_DIR}/lib/x64/SDL2main.lib")
    include_directories(${SDL2_INCLUDE_DIR})
    target_link_libraries(jacek-engine ${SDL2_LIBRARIES})
else ()
    set(SDL2_DIR "${PROJECT_SOURCE_DIR}/third_party/SDL-linux")
    set(SDL2_INCLUDE_DIR "${SDL2_DIR}/include")
    set(SDL2_LIBRARIES "${SDL2_DIR}/lib/libSDL2.a;${SDL2_DIR}/lib/libSDL2main.a")
    include_directories(${SDL2_INCLUDE_DIR})
    target_link_libraries(jacek-engine ${SDL2_LIBRARIES})
endif ()


# VK-BOOTSTRAP
message("Setting up VK-BOOTSTRAP...")
include_directories(third_party/vk-bootstrap)

# VOLK
message("Setting up VOLK...")
include_directories(third_party/volk)
target_link_libraries(jacek-engine volk)

# VulkanMemoryAllocator
message("Setting up VulkanMemoryAllocator...")
include_directories(third_party/VulkanMemoryAllocator)

# GLM
message("Setting up GLM...")
target_link_libraries(jacek-engine glm)

# Vulkan
message("Setting up Vulkan...")
target_link_libraries(jacek-engine Vulkan::Vulkan)

# Copy SDL DLLs as a post-build step
if (WIN32)
    message("Copying SDL2.dll to the target directory")
    add_custom_command(TARGET jacek-engine POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${SDL2_DIR}/lib/x64/SDL2.dll"
        $<TARGET_FILE_DIR:jacek-engine>
        )
else ()
    message("Copying libSDL2-2.0.so to the target directory")
    add_custom_command(TARGET jacek-engine POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${SDL2_DIR}/lib/libSDL2-2.0.so"
        $<TARGET_FILE_DIR:jacek-engine>
        )
endif()

# This is Vulkan shader compilation, a separate project in the solution
find_program(GLSL_VALIDATOR glslangValidator HINTS /usr/bin /usr/local/bin $ENV{VULKAN_SDK}/Bin/ $ENV{VULKAN_SDK}/Bin32/)

message("Vulkan shader compilation")
file(GLOB_RECURSE GLSL_SOURCE_FILES
        "${PROJECT_SOURCE_DIR}/shaders/*.frag"
        "${PROJECT_SOURCE_DIR}/shaders/*.vert"
        "${PROJECT_SOURCE_DIR}/shaders/*.comp"
        )

foreach(GLSL ${GLSL_SOURCE_FILES})
    message(STATUS "BUILDING SHADER")
    get_filename_component(FILE_NAME ${GLSL} NAME)
    set(SPIRV "${PROJECT_SOURCE_DIR}/shaders/${FILE_NAME}.spv")
    message(STATUS ${GLSL})
    add_custom_command(
            OUTPUT ${SPIRV}
            COMMAND ${GLSL_VALIDATOR} -V ${GLSL} -o ${SPIRV}
            DEPENDS ${GLSL})
    list(APPEND SPIRV_BINARY_FILES ${SPIRV})
endforeach(GLSL)

add_custom_target(
        Shaders
        DEPENDS ${SPIRV_BINARY_FILES}
)
