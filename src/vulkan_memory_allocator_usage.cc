// Because VMA is a single-header library, exactly one C++ file needs to include
// its private implementation, like this:

#include <cstdio> // vk_mem_alloc uses cstdio and some compilers do not include it implicitly

#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
