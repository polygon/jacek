// vulkan_guide.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#include <cassert>
#include <cstdint>
#include <cstdlib>

#include <memory>
#include <string>
#include <cstdio> // vk_mem_alloc uses cstdio and some compilers do not include it implicitly

#include <volk.h>

#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#include <vk_mem_alloc.h>

//we will add our main reusable types here
