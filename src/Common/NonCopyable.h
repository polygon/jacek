#pragma once

class NonCopyable
{
public:
	NonCopyable() = default;

private:
	NonCopyable(const NonCopyable&) {}
	void operator= (const NonCopyable&) {}
};