#pragma once
#include "TypeDefinitions.h"
#include "HelperMacros.h"
#include "NonCopyable.h"
//#include "Memory/MemoryBlock.h"

#include <string>
#include <cstring>
#include <filesystem>
#include <unordered_map>
#include <string_view>
//#include <xhash>
#include <type_traits>

using String = std::string;
using Path = std::filesystem::path;
using CString = const char*;
class Name;

////////////// TempString

// String allocated in temporary frame memory pool - valid only for 1 frame
/**class TempString
{
public:
	TempString() = default;
	explicit TempString(const char* str);
	explicit TempString(const String& str);
	explicit TempString(const Name& str);
	TempString(TempString&& other);
	TempString(const TempString& other);
	~TempString();

	void operator=(TempString&& other);
	void operator=(const TempString& other);
	void operator=(const char* str);
	void operator=(const String& str);
	void operator=(const Name& str);

	NO_DISCARD bool operator==(const TempString& other) const;
	NO_DISCARD bool operator==(const char* other) const;
	NO_DISCARD bool operator==(const String& other) const;
	NO_DISCARD bool operator==(const Name& other) const;

	NO_DISCARD char operator[](uint32 id) { return GetBuffer()[id]; }
	
	// TODO change to operator TempString
	NO_DISCARD operator String ()
	{
		return String((const char*)_block.address);
	}

	// returns number of stored characters
	NO_DISCARD uint32 GetSize() const { return _block.size; }

	// returns stored cstring
	NO_DISCARD const char* GetBuffer() const { return (char*)_block.address; }

	// Clears string and returns memory if allocated
	void Clear();

	void Invalidate()
	{
		_block.address = nullptr;
		_block.size = 0;
	}

	// TODO begin, end
	// TODO find

private:
	// cstring character sequence
	memory::Block _block;

	// Allocates memory for size characters,
	// if already assigned - asserts
	void Allocate(uint32 size);
};*/


using TempString = String;

////////////// Name

class Name
{
public:
	using NameId = uint64;
	constexpr static NameId INVALID_NAME_ID = 0;
private:

	static std::unordered_map<NameId, String>& GetHashedStrings()
	{
		static std::unordered_map<NameId, String> hashedStrings;
		return hashedStrings;
	}
	static std::unordered_map<String, NameId>& GetHashedIds()
	{
		static std::unordered_map<String, NameId> hashedIds;
		return hashedIds;
	}
	static NameId _nextFreeId;
public:
	Name() = default;
	Name(const char* nameString)
	{
		*this = nameString;
	}
	/**Name(const TempString& nameString)
	{
		*this = nameString;
	}*/
	Name(const String& nameString)
	{
		*this = nameString;
	}
	Name(const Name& other)
		:_nameId(other._nameId)
	{
	}

	/**Name& operator=(const TempString& nameString)
	{
		auto it = GetHashedIds().find(nameString.GetBuffer());
		if (it == GetHashedIds().end())
		{
			RE_ASSERT(_nextFreeId != 0 && "NameId pool overflow!");

			GetHashedIds().insert(std::make_pair(nameString, _nextFreeId));
			GetHashedStrings().insert(std::make_pair(_nextFreeId, nameString));

			_nameId = _nextFreeId;
			++_nextFreeId;
		}
		else
		{
			_nameId = it->second;
		}
		return *this;
	}*/
	Name& operator=(const char* nameString)
	{
		auto it = GetHashedIds().find(nameString);
		if (it == GetHashedIds().end())
		{
			YK_ASSERT(_nextFreeId != 0 && "NameId pool overflow!");

			GetHashedIds().insert(std::make_pair(nameString, _nextFreeId));
			GetHashedStrings().insert(std::make_pair(_nextFreeId, nameString));

			_nameId = _nextFreeId;
			++_nextFreeId;
		}
		else
		{
			_nameId = it->second;
		}
		return *this;
	}
	Name& operator=(const String& nameString)
	{
		auto it = GetHashedIds().find(nameString);
		if (it == GetHashedIds().end())
		{
			YK_ASSERT(_nextFreeId != 0 && "NameId pool overflow!");

			GetHashedIds().insert(std::make_pair(nameString, _nextFreeId));
			GetHashedStrings().insert(std::make_pair(_nextFreeId, nameString));

			_nameId = _nextFreeId;
			++_nextFreeId;
		}
		else
		{
			_nameId = it->second;
		}
		return *this;
	}
	Name& operator=(const Name& name)
	{
		_nameId = name._nameId;
		return *this;
	}

	NO_DISCARD bool operator == (const Name& name) const
	{
		return _nameId == name._nameId;
	}

	NO_DISCARD const String& GetString() const
	{
		static String invalidString = "";
		if (_nameId == INVALID_NAME_ID)
			return invalidString;
		return GetHashedStrings()[_nameId];
	}
	NO_DISCARD const void GetString(String& out) const
	{
		if (_nameId == INVALID_NAME_ID)
		{
			out = "";
			return;
		}
		out = GetHashedStrings()[_nameId];
	}

	operator NameId() const
	{
		return _nameId;
	}

	NO_DISCARD bool IsValid() const { return _nameId != INVALID_NAME_ID; }
	NO_DISCARD operator bool() const { return IsValid(); }

private:
	NameId _nameId {0};
};

// engine allocated string
// TODO make String be a TString(memory::SystemMemoryPool)
/**template<typename SourceMemoryPool = memory::SystemMemoryPool>
class TString
{
public:
	TString()
	{
		RE_ASSERT(false && "TString is not implemented yet");
		//static_assert(false, "TArray is not yet implemented");
	}
};

template<typename SourceMemoryPool = memory::SystemMemoryPool>
class TText
{
public:
	TText()
	{
		RE_ASSERT(false && "TText is not implemented yet");
		//static_assert(false, "TArray is not yet implemented");
	}
};*/

class StringSlice
{
public:
	StringSlice(const char* cstring)
		: _buffer(cstring)
		, _size(strlen(cstring))
	{
	}
	StringSlice(const char* buffer, uint32 size)
		: _buffer(buffer)
		, _size(size)
	{
	}
	StringSlice(const String& string)
		: _buffer(string.data())
		, _size(string.size())
	{
	}
	/**StringSlice(const TempString& tempString)
		: _buffer(tempString.GetBuffer())
		, _size(tempString.GetSize())
	{
	}*/
	StringSlice(const Name& name)
		: _buffer(name.GetString().data())
		, _size(name.GetString().size())
	{
	}
	/**StringSlice(const Path& path)
		: _buffer(path.string().data())
		, _size(path.string().size())
	{
		// TODO remove allocation
	}*/

	const char* _buffer{nullptr};
	uint32		_size{0};
};


inline bool operator<(Name lhs, Name rhs)
{
	return (Name::NameId)lhs < (Name::NameId)rhs;
}
inline bool operator<=(Name lhs, Name rhs)
{
	return (Name::NameId)lhs <= (Name::NameId)rhs;
}
inline bool operator>(Name lhs, Name rhs)
{
	return (Name::NameId)lhs > (Name::NameId)rhs;
}
inline bool operator>=(Name lhs, Name rhs)
{
	return (Name::NameId)lhs >= (Name::NameId)rhs;
}

namespace std
{
	template<>
	struct hash<Name>
	{
		std::size_t operator() (const Name& name) const
		{
			return hash<Name::NameId>()((Name::NameId)name);
		}
	};
}

////////////////// const char* operators
template<typename Type>
	requires(	std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, String> == false
			&&  std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
			&&	std::is_enum_v<Type> == false)
inline TempString operator+(const char* str, Type t)
{
	static std::stringstream stream;
	stream.str( "" );
	stream << str << t;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, String> == false
		&&   std::is_same_v<Type, const char*> == false
		&&	 std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
		&&	 std::is_enum_v<Type> == false	)
inline TempString operator+(Type t, const char* str)
{
	static std::stringstream stream;
	stream.str("");
	stream << t << str;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_enum_v<Type>)
inline TempString operator+(const char* str, Type t)
{
	static std::stringstream stream;
	stream.str("");
	stream << str << (uint64)t;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_enum_v<Type>)
inline TempString operator+(Type t, const char* str)
{
	static std::stringstream stream;
	stream.str("");
	stream << t << (uint64)str;
	return TempString(stream.str());
}

////////////////// String operators
template<typename Type>
	requires(std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, String> == false
		&&   std::is_same_v<Type, const char*> == false
		&&	 std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
		&&	 std::is_enum_v<Type> == false	)
inline TempString operator+(const String& str, Type t)
{
	static std::stringstream stream;
	stream.str("");
	stream << str << t;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, String> == false
		&&   std::is_same_v<Type, const char*> == false
		&&	 std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
		&&	 std::is_enum_v<Type> == false	)
inline void operator+=(String& str, Type t)
{
	static std::stringstream stream;
	stream.str("");
	stream << t;
	str += t;
}

template<typename Type>
	requires(std::is_enum_v<Type>)
inline TempString operator+(const String& str, Type t)
{
	static std::stringstream stream;
	stream.str("");
	stream << str << (uint64)t;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, String> == false
		&&   std::is_same_v<Type, const char*> == false
		&&	 std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
		&&	 std::is_enum_v<Type> == false	)
inline TempString operator+(Type t, const String& str)
{
	static std::stringstream stream;
	stream.str("");
	stream << t << str;
	return TempString(stream.str());
}


template<typename Type>
	requires(std::is_enum_v<Type>)
inline TempString operator+(Type t, const String& str)
{
	static std::stringstream stream;
	stream.str("");
	stream << t << (uint64)str;
	return TempString(stream.str());
}

////////////////// Temp String operators

/**template<typename Type>
	requires(std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
		&&	 std::is_enum_v<Type> == false )
inline TempString operator+(const TempString& str, Type t)
{
	static std::stringstream stream;
	stream.str( "" );
	stream << str.GetBuffer() << t;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_same_v<std::remove_reference_t<std::remove_const_t<Type>>, TempString> == false
		&&	 std::is_enum_v<Type> == false )
inline TempString operator+(Type t, const TempString& str)
{
	static std::stringstream stream;
	stream.str( "" );
	stream << t << str.GetBuffer();
	return TempString(stream.str());
}

inline TempString operator+(const TempString& str, const TempString& t)
{
	static std::stringstream stream;
	stream.str( "" );
	stream << str.GetBuffer() << t.GetBuffer();
	return TempString(stream.str());
}*/

template<typename Type>
inline void operator+=(TempString& str, Type t)
{
	str = str + t;
}

/**
template<typename Type>
	requires(std::is_enum_v<Type>)
inline TempString operator+(const TempString& str, Type t)
{
	static std::stringstream stream;
	stream.str( "" );
	stream << str.GetBuffer() << (uint64)t;
	return TempString(stream.str());
}

template<typename Type>
	requires(std::is_enum_v<Type>)
inline TempString operator+(Type t, const TempString& str)
{
	static std::stringstream stream;
	stream.str( "" );
	stream << (uint64)t << str.GetBuffer();
	return TempString(stream.str());
}/**/
