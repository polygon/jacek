#pragma once
/**#include <Engine/Common/Memory/FrameMemoryPool.h>

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
TTempArray<Type>::TTempArray(TSlice<Type> slice)
{
	Allocate( slice.length);
	_size = slice.length;
	memcpy(_buffer, slice.data, _size);
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
TTempArray<Type>::TTempArray(TTempArray&& other)
	: _size( other._size)
	, _capacity(other._capacity)
	, _buffer(other._buffer)
{
	other.Invalidate();
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
TTempArray<Type>::TTempArray(TSlice<Type> slice, uint32 allocateForAdditional)
{
	Allocate((allocateForAdditional + slice.length));
	_size = slice.length;
	memcpy(_buffer, slice.data, _size);
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
TTempArray<Type>::TTempArray(uint32 allocateFor)
{
	Allocate(allocateFor);
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
TTempArray<Type>::~TTempArray()
{
	Clear();
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
inline void TTempArray<Type>::Clear()
{
	if (GetBuffer() != nullptr)
	{
		memory::FrameMemoryPool::Get().Free({_buffer, _capacity * sizeof(Type)});
		Invalidate();
	}
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
inline void TTempArray<Type>::Invalidate()
{
	_buffer = nullptr;
	_size = 0;
	_capacity = 0;
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
int32 TTempArray<Type>::Find(const Type& e) const
{
	for (int32 i = 0; i < _size - 1; ++i)
	{
		if (e == _buffer[i])
			return i;
	}
	return -1;
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
void TTempArray<Type>::RemoveBack(uint32 id)
{
	RE_ASSERT(_size > 0);
	--_size;
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
void TTempArray<Type>::Remove(uint32 id)
{
	RE_ASSERT(id < _size);
	for (int32 i = id; i < _size - 1; ++i)
	{
		_buffer[i] = _buffer[i + 1];
	}
	--_size;
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
void TTempArray<Type>::Add(const Type&& e)
{
	RE_ASSERT(_size < _capacity);
	_buffer[_size] = std::move(e);
	++_size;
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
void TTempArray<Type>::Add(const Type& e)
{
	RE_ASSERT(_size < _capacity);
	_buffer[_size] = e;
	++_size;
}

template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
void TTempArray<Type>::Allocate(uint32 size)
{
	RE_ASSERT(GetBuffer() == nullptr);
	_buffer = (Type*)memory::FrameMemoryPool::Get().Allocate(size * sizeof(Type)).address;
	_capacity = size;
}
/**/