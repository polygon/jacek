#pragma once

#include "../StringUtility.h"
//#include "../Loging.h"
#include <fstream>
#include <iostream>
//#include "yaml-cpp/yaml.h"


///////////////////////////////
///////// Serialize

#ifdef RTTI_ENABLE_SERIALIZATION
inline bool SerializeToFile(const YAML::Emitter& emitter, const Path& filePath )
{
	std::ofstream file;
	file.open(filePath);

	if (file)
	{
		file << emitter.c_str();
		file.close();
		return true;
	}

	// log error
	std::cerr << "Falied to open the file: \"" << filePath << "\" to save\n";
	std::cerr << "badbit " << file.badbit << ", failbit" << file.failbit << ", eof" << file.eofbit;

	return false;
}

inline bool SerializeToString(const YAML::Emitter& emitter, String& serializationTarget)
{
	if (emitter.good())
	{
		serializationTarget = emitter.c_str();
		return true;
	}

	return false;
}

template<typename Type>
inline void SerializeDirectValue(YAML::Emitter& emitter, const Type& value)
{
	emitter << value;
}

template<>
inline void SerializeDirectValue<Name>(YAML::Emitter& emitter, const Name& value)
{
	emitter.Write(value.GetString());
}

template<>
inline void SerializeDirectValue<Path>(YAML::Emitter& emitter, const Path& value)
{
	emitter.Write(value.string());
}

template<>
inline void SerializeDirectValue<uint8>(YAML::Emitter& emitter, const uint8& value)
{
	uint16 v = value;
	SerializeDirectValue(emitter, v);
}

///////// Map

class YamlMapSerializer
{
public:
	YamlMapSerializer( YAML::Emitter& emitter )
		: _emitter( emitter )
	{
		emitter << YAML::BeginMap;
	}
	~YamlMapSerializer()
	{
		_emitter << YAML::EndMap;
	}

	void SerializeElement( const String& key )
	{
		_emitter << YAML::Key << key << YAML::Value;
	}

	template<typename Type>
	inline void SerializeElement( const String& key, const Type& value )
	{
		SerializeElement( key );
		SerializeDirectValue<Type>(_emitter, value );
	}

private:
	YAML::Emitter& _emitter;
};


///////// Sequence


class YamlSequenceSerializer
{
public:
	YamlSequenceSerializer(YAML::Emitter& emitter)
		: _emitter(emitter)
	{
		_emitter << YAML::BeginSeq;
	}
	~YamlSequenceSerializer()
	{
		_emitter << YAML::EndSeq;
	}

	void SerializeElement()
	{
		_emitter << YAML::Value;
	}

	template<typename Type>
	inline void SerializeElement(const Type& value)
	{
		SerializeElement();
		SerializeDirectValue<Type>(_emitter, value);
	}

private:
	YAML::Emitter& _emitter;
};

///////////////////////////////
///////// Deserialize

inline YAML::Node DeserializeFromFile(const Path& filePath)
{
	if (std::filesystem::exists(filePath) == false)
	{
		LOG_WARNING("Trying to open non existent file \"" + filePath + "\"");
		return {};
	}
	return std::move(YAML::LoadFile(std::move(filePath.string())));
}

inline YAML::Node DeserializeFromString(const std::string_view& deserializationTarget)
{
	return std::move(YAML::Load(String(deserializationTarget).c_str()));
}


template<typename Type>
inline void DeserializeDirectValue(const YAML::Node& node, Type& value)
{
	value = node.as<Type>();
}

template<>
inline void DeserializeDirectValue<Name>(const YAML::Node& node, Name& value)
{
	value = node.as<String>();
}

template<>
inline void DeserializeDirectValue<Path>(const YAML::Node& node, Path& value)
{
	value = node.as<String>();
}

template<>
inline void DeserializeDirectValue<uint8>(const YAML::Node& node, uint8& value)
{
	uint16 v;
	DeserializeDirectValue(node, v);
	value = (uint8)v;
}


///////// Map

class YamlMapDeserializer
{
public:
	YamlMapDeserializer(const YAML::Node& node)
		: _node(node)
	{
		RE_ASSERT(_node);
		RE_ASSERT(_node.IsMap());
	}

	YAML::Node DeserializeElement(const String& key)
	{
		auto n = _node[key];
		return n;
	}

	template<typename Type>
	inline YAML::Node DeserializeElement( const String& key, Type& value)
	{
		YAML::Node elementNode = DeserializeElement( key);
		if (!elementNode)
		{
			std::cerr << "File is ill formed\n";
		}
		else
		{
			DeserializeDirectValue<Type>(elementNode, value);
		}
		return elementNode;
	}

	template<typename Type>
	inline YAML::Node DeserializeElement(const String& key, Type& value, const Type& defaultValue)
	{
		YAML::Node elementNode = DeserializeElement( key);
		if (elementNode)
			DeserializeDirectValue<Type>(elementNode, value);
		else
			value = defaultValue;

		return elementNode;
	}


private:
	const YAML::Node& _node;
};

///////// Sequence

class YamlSequenceDeserializer
{
public:
	YamlSequenceDeserializer(const YAML::Node& node)
		: _node(node)
	{
		RE_ASSERT(_node);
		RE_ASSERT(_node.IsSequence());

		_iterator = _node.begin();
	}

	bool DeserializeElement(YAML::Node& outElementNode)
	{
		if (_iterator == _node.end())
			return false;

		const YAML::Node& it = *_iterator;
		outElementNode = it;
		++_iterator;
		return true;
	}

	template<typename Type>
	inline bool DeserializeElement(Type& outValue)
	{
		YAML::Node elementNode;
		bool shouldContinue = DeserializeElement(elementNode);
		if (shouldContinue == false)
			return false;

		DeserializeDirectValue<Type>(elementNode, outValue);

		return true;
	}

private:
	const YAML::Node& _node;
	YAML::const_iterator _iterator;
};


#endif // RTTI_ENABLE_SERIALIZATION
