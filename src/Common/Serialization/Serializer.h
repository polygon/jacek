#pragma once
#include "Serialization.h"
#include <functional>

namespace YAML
{
    class Emitter;
    class Node;
}

template<typename SerializationContext>
class TISerializerBase
{
public:
	virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const = 0;
	virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const = 0;
};

template<typename SerializationContext>
class TSerializerNoSerialize : public TISerializerBase<SerializationContext>
{
	virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override { }
	virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override { }
};

template<typename SerializationContext>
class TSerializerLambda : public TISerializerBase<SerializationContext>
{
public:
	using SerializeDelegate = std::function<void(YAML::Emitter&, SerializationContext)>;
	using DeserializeDelegate = std::function<void(const YAML::Node&, SerializationContext)>;

	TSerializerLambda(SerializeDelegate serializeDelegate, DeserializeDelegate deserializeDelegate)
		: _serializeDelegate(serializeDelegate)
		, _deserializeDelegate(deserializeDelegate)
	{
	}

	virtual void Serialize(YAML::Emitter& emitter, SerializationContext context) const override
	{
		_serializeDelegate(emitter, context);
	}
	virtual void Deserialize(const YAML::Node& node, SerializationContext context) const override
	{
		_deserializeDelegate(node, context);
	}

private:
	SerializeDelegate	_serializeDelegate;
	DeserializeDelegate	_deserializeDelegate;
};
