#pragma once
#include "TypeDefinitions.h"
#include "NonCopyable.h"
#include "HelperMacros.h"
#include <vector>
#include <array>
#include <initializer_list>

/**template<typename Type, typename SourceMemoryPool = memory::SystemMemoryPool>
using TDynArray = //std::conditional_t< std::is_same_v<SourceMemoryPool, memory::SystemMemoryPool>,
	std::vector<Type>//,
	//std::vector<Type, memory::StdAllocator<Type, SourceMemoryPool>>
//>
;*/
template<typename Type>//, typename SourceMemoryPool = memory::SystemMemoryPool>
using TDynArray = std::vector<Type>;
/**template<typename Type, typename SourceMemoryPool = memory::SystemMemoryPool>
using TDynArray = std::conditional_t < std::is_same_v<SourceMemoryPool, memory::SystemMemoryPool>,
	std::vector<Type>,
	std::vector<Type, memory::StdAllocator<Type, SourceMemoryPool>>
>;*/

template<typename Type, std::size_t size>
using TStaticArray = std::array<Type, size>;

template<typename T>
class TSlice;


// stores data for 1 frame
// Is invalidated after the frame finishes
// Warning - do not use for types that are not default destructible - it will not call destructor of the class
/**
template<typename Type>
	requires(std::is_trivially_destructible_v<Type> 
		&&	std::is_constructible_v<Type>
		)
class TTempArray : public NonCopyable
{
public:
	TTempArray() = default;
	TTempArray(TSlice<Type> slice);
	TTempArray(TTempArray&& other);

	TTempArray(TSlice<Type> slice, uint32 allocateForAdditional);
	TTempArray(uint32 allocateFor);
	~TTempArray();

	void Add(const Type& e);
	
	void Add(const Type&& e);

	void Remove(uint32 id);

	void RemoveBack(uint32 id);

	NO_DISCARD int32 Find(const Type& e) const;

	NO_DISCARD Type& operator[](int32 id) { return _buffer[id]; }

	NO_DISCARD const Type& operator[](int32 id) const { return _buffer[id]; }

	NO_DISCARD uint32 GetSize() const { return _size; }

	NO_DISCARD uint32 GetCapacity() const { return _size; }

	NO_DISCARD Type* GetBuffer() const { return _buffer; }

	void Clear();

	void Invalidate();

	// TODO begin, end

private:
	Type* _buffer{ nullptr };
	uint32 _size{ 0 };
	uint32 _capacity{ 0 };

	// Allocates memory for size elements,
	// if already assigned - asserts
	void Allocate(uint32 size);
};

*/

template<typename T>
class TSlice
{
public:
	TSlice(T* ptr, int length)
		: data(ptr), length(length)
	{}
	TSlice(const std::initializer_list<T>& v)
		: data(&*(T*)v.begin())
		, length(v.size())
	{}
	TSlice(const TDynArray<T>& v)
		: data((T*)v.data())
		, length(v.size())
	{}
	template<int size>
	TSlice(const TStaticArray<T, size>& v)
		: data((T*)v.data())
		, length(size)
	{}
	/**TSlice(const TTempArray<T>& v)
		: data(v.GetBuffer())
		, length(v.GetSize())
	{}*/

	T* data;
	int length;
};


/**template<typename T>//, typename SourceMemoryPool = memory::SystemMemoryPool>
class TArray
{
public:
	TArray()
	{
		YK_ASSERT(false && "TArray is not implemented yet");
		//static_assert(false, "TArray is not yet implemented");
	}
};/**/
//TODO dynamic array mimicing std::vector, using engine allocators

#include "ArrayUtility.inl"
