#pragma once
#include "rttiEnum.h"

namespace rtti
{ 
	/// /brief type template for ease of creation of nonabstract type objects
	template<typename Type>
	class TEnumType : public EnumType
	{
	public:
		TEnumType(const char* name)
			: EnumType(name, sizeof(Type))
		{
			static_assert(std::is_enum<Type>::value
				, SASSERT_ERROR_MESSAGE("TEnumType", "EnumType can be created only for enums")
				);
		}

		virtual void* CreateNew() const override
		{
			return new Type();
		}
		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				return new(memory) Type{};
			else
				return nullptr;
		}
		virtual void  Destroy(void* object) const override
		{
			delete ((Type*)object);
		}
		virtual void DestroyInPlace(void* object) const override
		{
			((Type*)object)->~Type();
		}
	protected:
		virtual void* DuplicateNew(const void* original) const override
		{
			return new Type(*((const Type*)original));// Call copy constructor
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			return new Type(std::move(*((const Type*)original)));// Call move constructor
		}
	};
}