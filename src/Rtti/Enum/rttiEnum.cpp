#include "rttiEnum.h"
#include "../Class/rttiProperty.h"
//#include <Engine/Editor/BasicTypeDrawers.h>

namespace rtti
{
    template<typename Ty>
    static inline bool DrawEnum(const EnumType& enumV, TypeDrawingContext& context, uint64* _enumId)
    {
#ifdef RTTI_ENABLE_DRAWING
        bool valueChanged = false;

        Ty* enumId = (Ty*)_enumId;
        *enumId = *_enumId;

        if (*enumId >= 0 && *enumId < (int64)enumV.GetOptionsCount())
        {
            if (ImGui::BeginCombo(IMGUI_ID(""), enumV.GetOption(*enumId)->GetDisplayName()))
            {
                for (size_t i = 0; i < enumV.GetOptionsCount(); i++)
                {
                    const bool is_selected = (*enumId == i);
                    if (ImGui::Selectable(enumV.GetOption(i)->GetDisplayName(), is_selected))
                    {
                        *(Ty*)enumId = (Ty)i;
                        valueChanged = true;
                    }
                    // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                    if (is_selected)
                        ImGui::SetItemDefaultFocus();
                }
                ImGui::EndCombo();
            }
        }

        return valueChanged;
#else
        return false;
#endif // RTTI_ENABLE_DRAWING
    }

	bool EnumType::DrawType(TypeDrawingContext& context) const
	{
        uint64* enumId = (uint64*)AllocateTempVarriable(context);
        
        bool modified = false;
        if (GetTypeSize() == sizeof(uint8))
            modified = DrawEnum<uint8>(*this, context, enumId);
        else if (GetTypeSize() == sizeof(uint16))
            modified = DrawEnum<uint16>(*this, context, enumId);
        else if (GetTypeSize() == sizeof(uint32))
            modified = DrawEnum<uint32>(*this, context, enumId);
        if (GetTypeSize() == sizeof(uint64))
            modified = DrawEnum<uint64>(*this, context, enumId);

        FreeTempVarriable(context, enumId, modified);

        return modified;
	}
    void EnumType::Serialize(YAML::Emitter& emitter, SerializationContext context) const
    {
#ifdef RTTI_ENABLE_SERIALIZATION
#define TYPE_DEPENDENT(Type)                                   \
        if(GetTypeSize() == sizeof(Type))                      \
        {                                                      \
            int64 enumId = *((Type*)context.objectStart);      \
                                                               \
            if (enumId >= 0 && enumId < (int64)GetOptionsCount())     \
                emitter << GetOption(enumId)->GetNativeName(); \
        }

        TYPE_DEPENDENT(uint8) 
        else TYPE_DEPENDENT(uint16)
        else TYPE_DEPENDENT(uint32)
        else TYPE_DEPENDENT(uint64)
        else LOG_ERROR("Invalid enum type");

#undef TYPE_DEPENDENT
#endif
    }
    void EnumType::Deserialize(const YAML::Node& node, SerializationContext context) const
    {
#ifdef RTTI_ENABLE_SERIALIZATION
#define TYPE_DEPENDENT(Type)                                                        \
        if(GetTypeSize() == sizeof(Type))                                           \
        {                                                                           \
            std::string parsedEnum = node.as<std::string>();                        \
            auto option = FindOptionByNativeName(parsedEnum.c_str());               \
            if(option == nullptr)                                                   \
            {                                                                       \
                LOG_ERROR( "Invalid enum name" );                                   \
            }                                                                       \
            int64 parsedEnumId = option->GetValue();                                \
                                                                                    \
            if (parsedEnumId >= 0 && parsedEnumId < (int64)GetOptionsCount())       \
            {                                                                       \
                Type* enumId = (Type*)context.objectStart;                          \
                *enumId = (Type)parsedEnumId;                                       \
            }                                                                       \
            else LOG_ERROR( "loaded invalid enum value" );                          \
        }

        TYPE_DEPENDENT(uint8)
        else TYPE_DEPENDENT(uint16)
        else TYPE_DEPENDENT(uint32)
        else TYPE_DEPENDENT(uint64)
        else LOG_ERROR("Invalid enum type");

#undef TYPE_DEPENDENT
#endif
    }


    
}

namespace editor
{
    bool BitEnumDrawer::DrawType(rtti::TypeDrawingContext& context) const
    {
#ifdef RTTI_ENABLE_DRAWING
        rtti::EnumType* type = (rtti::EnumType*)context.type;
        void* temp = nullptr;
        uint64 enumValue;
        bool valueChanged = false;
#define TYPE_DEPENDENT(Type)                                                        \
        if(type->GetTypeSize() == sizeof(Type))                                     \
        {                                                                           \
            temp = AllocateTempVarriable(context);                                  \
            enumValue = *(Type*)temp;                                               \
        }

        TYPE_DEPENDENT(uint8)
        else TYPE_DEPENDENT(uint16)
        else TYPE_DEPENDENT(uint32)
        else TYPE_DEPENDENT(uint64)
        else LOG_ERROR("Invalid enum type");

#undef TYPE_DEPENDENT

        const char* preview = enumValue == 0 ? "None" : "Multiple Options";
        for (int i = 0; i < type->GetOptionsCount(); ++i)
        {
            if (type->GetOption(i)->GetValue() == enumValue)
                preview = type->GetOption(i)->GetDisplayName();
        }

        ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
        
        if (editor::BeginDrawEnum(IMGUI_ID(""), preview))
        {
            for (int i = 0; i < type->GetOptionsCount(); ++i)
            {
                uint64 optionValue = type->GetOption(i)->GetValue();
                const bool isSelected = (enumValue & optionValue) != 0;

                if (editor::DrawEnumElement(IMGUI_ID(type->GetOption(i)->GetDisplayName()), isSelected))
                {
#define TYPE_DEPENDENT(Type)                                                \
                    if(type->GetTypeSize() == sizeof(Type))                 \
                    {                                                       \
                        if(isSelected)                                      \
                            *(Type*)temp &= ~optionValue;    \
                        else                                                \
                            *(Type*)temp |= optionValue;     \
                    }

                    TYPE_DEPENDENT(uint8)
                    else TYPE_DEPENDENT(uint16)
                    else TYPE_DEPENDENT(uint32)
                    else TYPE_DEPENDENT(uint64)
                    else LOG_ERROR("Invalid enum type");

                    valueChanged = true;

#undef TYPE_DEPENDENT
                }
            }
            editor::EndDrawEnum();
        }

        FreeTempVarriable(context, temp, valueChanged);
        return valueChanged;
#else
        return false;
#endif
    }
}
