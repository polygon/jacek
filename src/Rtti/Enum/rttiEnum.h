#pragma once

#include "../Type/rttiTypeBase.h"
#include "../Drawing/rttiTypeDrawer.h"
#include "../Serialization/rttiSerialization.h"

namespace rtti
{
	// TODO enum flags display

	/// /brief possible value of an enum 
	/// 
	/// as a value is stored int64 but _it can represent any of enum sizes
	class EnumOption
	{
	public:
		EnumOption(const char* nativeName, int64 value)
			: _nativeName(nativeName)
			, _displayName(nativeName)
			, _value(value)
			, _tooltip("")
		{
		}

		const char* GetNativeName() const { return _nativeName; }

		const char* GetDisplayName() const { return _displayName; }
		EnumOption& SetDisplayName(const char* name) { _displayName = name; return *this; }

		NO_DISCARD const char* GetTooltip() const { return _tooltip; }
		EnumOption& SetTooltip(const char* name) { _tooltip = name; return *this; }

		uint64 GetValue() const { return _value; }
		EnumOption& SetValue(uint64 value) { _value = value; return *this; }

	private:
		const char* _nativeName;
		const char* _displayName;
		const char* _tooltip;
		int64		_value;
	};

	class EnumType : public TypeBase, public TypeDrawerBase, public ITypeSerializerBase
	{
	public:
		EnumType(const char* name, uint16 size)
			: TypeBase( name, size)
		{
			SetTypeSerializer(this);
			SetTypeDrawer(this);
		}

		EnumOption& AddOption(const char* optionName, int64 value)
		{
			_options.push_back(EnumOption(optionName, value));
			return _options.back();
		}

		const EnumOption* GetOption(size_t i) const
		{
			if (i >= _options.size())
			{
				return nullptr;
			}
			return &_options[i];
		}
		const EnumOption* FindOptionByNativeName(const char* nativeName) const
		{
			for (int i = 0; i < _options.size(); ++i)
			{
				if (strcmp(nativeName, _options[i].GetNativeName()) == 0)
				{
					return &_options[i];
				}
			}
			return nullptr;
		}
		const EnumOption* FindOptionByDisplayName(const char* displayName) const
		{
			for (int i = 0; i < _options.size(); ++i)
			{
				if (strcmp(displayName, _options[i].GetDisplayName()) == 0)
				{
					return &_options[i];
				}
			}
			return nullptr;
		}

		size_t GetOptionsCount() const { return _options.size(); }

	public: // Drawing

		virtual bool DrawType(TypeDrawingContext& context) const override;
	
	public: // Serializing

		virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override;
		virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override;



	private:
		TDynArray<EnumOption>	_options;

	};

	/// /brief accessor of class types
	template<typename Type>
		requires(std::is_enum_v<Type>)
	inline EnumType* GetEnumDescriptor()
	{
		RE_ASSERT((dynamic_cast<EnumType*>(GetTypeDescriptor<Type>()) != nullptr));
		return (EnumType*)GetTypeDescriptor<Type>();
	}

	template<typename Type>
		requires(std::is_enum_v<Type>)
	inline NO_DISCARD const char* EnumToDisplayName(Type enumValue)
	{
		return GetEnumDescriptor<Type>()->GetOption((int64)enumValue)->GetDisplayName();
	}

	template<typename Type>
		requires(std::is_enum_v<Type>)
	inline NO_DISCARD const char* EnumToNativeName(Type enumValue)
	{
		return GetEnumDescriptor<Type>()->GetOption((int64)enumValue)->GetNativeName();
	}

	template<typename Type>
		requires(std::is_enum_v<Type>)
	inline NO_DISCARD const char* EnumToTooltip(Type enumValue)
	{
		return GetEnumDescriptor<Type>()->GetOption((int64)enumValue)->GetTooltip();
	}
}

namespace editor
{
	class BitEnumDrawer : public rtti::TypeDrawerBase
	{
	public:

		virtual bool DrawType(rtti::TypeDrawingContext& context) const override;

	private:

	};
}
