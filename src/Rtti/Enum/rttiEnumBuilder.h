#pragma once
#include "rttiTEnum.h"

namespace rtti
{
    class EnumType;

    class EnumBuilder
    {
    public:
        EnumBuilder(EnumType* typeDescriptor, std::function<void(EnumType* typeDescriptor)> initializationFunction)
        {
            initializationFunction(typeDescriptor);
        }
    };
}


/// /brief Registers an enum into the reflection system
/// 
/// inside _Body expression define enum propertities - mostly declare enum options
/// @warrning do not put this macro inside any namespace
#define RTTI_DEFINE_ENUM(_Type, _Body)                                                                          \
template<>                                                                                                      \
struct rtti::Internal::TypeDescriptor<_Type>                                                                    \
{                                                                                                               \
    TypeBase* operator()()                                                                                      \
    {                                                                                                           \
        static_assert(std::is_enum<_Type>(), SASSERT_ERROR_MESSAGE("RTTI_DEFINE_ENUM", "Type is not an enum")); \
        using Type = _Type;                                                                                     \
        static TEnumType<_Type>	    RTTI_DESCRIPTOR_NAME(YK_STRINGIFY(_Type));                                  \
        static EnumBuilder	        RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](EnumType* RTTI_DESCRIPTOR_NAME) \
        {                                                                                                       \
            RTTI_DESCRIPTOR_NAME->SetTypeDrawer(RTTI_DESCRIPTOR_NAME);									        \
            _Body                                                                                               \
            RTTI_DESCRIPTOR_NAME->FinalizeInitialization();                                                     \
        });                                                                                                     \
        return &RTTI_DESCRIPTOR_NAME;                                                                           \
    }                                                                                                           \
};

/// /brief declares a new option into enum
///
/// use _it only inside RTTI_DEFINE_ENUM body declaration
/// @optionName is a name of possible enum value
#define RTTI_ENUM_OPTION(optionName) \
    RTTI_DESCRIPTOR_NAME->AddOption(#optionName, (int64)Type:: optionName)
