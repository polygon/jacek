#pragma once
#include "../RttiCore.h"

namespace rtti
{
	class SerializationContext
	{
	public:
		SerializationContext(void* objectToDeserializePtr)
			: objectStart(objectToDeserializePtr)
			, property(nullptr)
		{
			YK_ASSERT((objectStart && "SerializationContext.objectToDeserializePtr can\'t be null, please assign a valid object address"));
		}
		void* objectStart;
		const class Property* property;
		int offset = 0;
	};

	using ITypeSerializerBase = TISerializerBase<rtti::SerializationContext>;

	using TypeSerializerNoSerialize = TSerializerNoSerialize<SerializationContext>;

	template<typename Type>
	class TTypeSerializerCast : public ITypeSerializerBase
	{
	public:
		virtual void Serialize(YAML::Emitter& emitter, SerializationContext context) const override
		{
#ifdef RTTI_ENABLE_SERIALIZATION
			emitter << YAML::Value;
			SerializeDirectValue(emitter, *(Type*)context.objectStart);
#endif
		}
		virtual void Deserialize(const YAML::Node& node, SerializationContext context) const override
		{
#ifdef RTTI_ENABLE_SERIALIZATION
			Type* v = (Type*)context.objectStart;
			DeserializeDirectValue(node, *v);
#endif
		}
	};

	class TTypeSerializerLambda : public ITypeSerializerBase
	{
	public:
		using SerializeDelegate = std::function<void(YAML::Emitter&, SerializationContext)>;
		using DeserializeDelegate = std::function<void(const YAML::Node&, SerializationContext)>;

		TTypeSerializerLambda(SerializeDelegate serializeDelegate, DeserializeDelegate deserializeDelegate)
			: _serializeDelegate(serializeDelegate)
			, _deserializeDelegate(deserializeDelegate)
		{
		}


		virtual void Serialize(YAML::Emitter& emitter, SerializationContext context) const override
		{
			_serializeDelegate(emitter, context);
		}
		virtual void Deserialize(const YAML::Node& node, SerializationContext context) const override
		{
			_deserializeDelegate(node, context);
		}

	private:
		SerializeDelegate	_serializeDelegate;
		DeserializeDelegate	_deserializeDelegate;
	};

}
