#pragma once
#include "rttiFunction.h"

namespace rtti
{

	class FunctionLibrary : public Singleton<FunctionLibrary>
	{
		using FunctionPtr = std::unique_ptr<FunctionTypeBase>;
		using GlobalFunctionsArray = TDynArray<FunctionPtr>;
	public:

		template<typename FunctionSignature>
		FunctionTypeBase* AddGlobalFunction(const char* functionName, FunctionSignature function)
		{
			auto rttiFunction = new rtti::TFunctionType(functionName, function);
			_globalFunctions.push_back(FunctionPtr(rttiFunction));
			return _globalFunctions.back().get();
		}

		auto GetGlobalFunctionsIterator() const
		{
			return TypeIterator<GlobalFunctionsArray, void>(_globalFunctions);
		}

		NO_DISCARD FunctionTypeBase* GetFunctionByDisplayName(const char* displayName) const
		{
			for (auto& it : _globalFunctions)
				if (strcmp(it->GetDisplayName(), displayName) == 0)
					return it.get();
			
			return nullptr;
		}

		NO_DISCARD FunctionTypeBase* GetFunctionByNativeName(const char* nativeName) const
		{
			for (auto& it : _globalFunctions)
				if (strcmp(it->GetNativeName(), nativeName) == 0)
					return it.get();

			return nullptr;
		}

	private:
		GlobalFunctionsArray	_globalFunctions;
	};

}

#define RTTI_GLOBAL_FUNCTION(function, _Body) \
STATIC_INITIALIZATION_BEGIN	\
	auto RTTI_DESCRIPTOR_NAME = rtti::FunctionLibrary::Get().AddGlobalFunction( RE_STRINGIFY(function), function); \
	_Body \
STATIC_INITIALIZATION_END

#define RTTI_FUNCTION_ARGUMENT_NAME( argId, name )\
	RTTI_DESCRIPTOR_NAME->GetArgumentProperty(argId).displayName = name
#define RTTI_FUNCTION_ARGUMENT_TOOLTIP( argId, name )\
	RTTI_DESCRIPTOR_NAME->GetArgumentProperty(argId).tooltip = name


#define RTTI_FUNCTION_RESULT_NAME( name )\
	RTTI_DESCRIPTOR_NAME->GetResultProperty().displayName = name
#define RTTI_FUNCTION_RESULT_TOOLTIP( name )\
	RTTI_DESCRIPTOR_NAME->GetResultProperty().tooltip = name
