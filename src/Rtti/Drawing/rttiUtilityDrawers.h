#pragma once
#include "rttiTypeDrawer.h"
#include "../Function/rttiFunction.h"

namespace rtti
{
	
	class TypeDrawerSpace : public TypeDrawerBase
	{
	public:

		virtual bool DrawHeader(TypeDrawingContext& context) const override { return true; }
		virtual bool DrawType(TypeDrawingContext& context) const override;
	};

	class TypeDrawerSameLine : public TypeDrawerBase
	{
	public:

		virtual bool DrawHeader(TypeDrawingContext& context) const override { return true; }
		virtual bool DrawType(TypeDrawingContext& context) const override;
	};

	class TypeDrawerInfoBox : public TypeDrawerBase
	{
	public:
		TypeDrawerInfoBox(const char* message)
			: _message(message)
		{

		}

		virtual bool DrawHeader(TypeDrawingContext& context) const override { return true; }
		virtual bool DrawType(TypeDrawingContext& context ) const override;

	private:
		const char* _message;
	};

	class TypeDrawerButton : public TypeDrawerBase
	{
	public:
		TypeDrawerButton(const char* label, rtti::FunctionTypeBase* function)
			: _label(label)
			, _function(function)
		{
		}

		virtual bool DrawHeader(TypeDrawingContext& context) const override { return true; }
		virtual bool DrawType(TypeDrawingContext& context) const override;

	private:
		const char* _label;
		rtti::FunctionTypeBase* _function;
	};


#define RTTI_SPACE()								\
	{												\
		static rtti::TypeDrawerSpace  drawer;		\
		RTTI_DESCRIPTOR_NAME->AddDrawer(&drawer);	\
	}

#define RTTI_SAME_LINE()								\
	{													\
		static rtti::TypeDrawerSameLine  drawer;		\
		RTTI_DESCRIPTOR_NAME->AddDrawer(&drawer);		\
	}

#define RTTI_INFOBOX(info)								\
	{													\
		static rtti::TypeDrawerInfoBox  drawer(info);	\
		RTTI_DESCRIPTOR_NAME->AddDrawer(&drawer);		\
	}

#define RTTI_BUTTON(_label, _function)																			\
	{																											\
		rtti::FunctionTypeBase* function = RTTI_DESCRIPTOR_NAME->FindMemberFunctionByNativeName(#_function);	\
		if(function == nullptr)																					\
		{																										\
			function = RTTI_MEMBER_FUNCTION(_function);															\
		}																										\
		static rtti::TypeDrawerButton drawer(_label, function);													\
		RTTI_DESCRIPTOR_NAME->AddDrawer(&drawer);																\
	}
	/**/
// TODO 
#define RTTI_SHOW_IF() static_assert(false, "RTTI_SHOW_IF is not implemented yet");
#define RTTI_HIDE_IF() static_assert(false, "RTTI_HIDE_IF is not implemented yet");
}