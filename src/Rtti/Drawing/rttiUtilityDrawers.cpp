#include "rttiUtilityDrawers.h"
//#include <Engine/Editor/EditorWidgets.h>

namespace rtti
{
	
	bool TypeDrawerSpace::DrawType(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		ImGui::NewLine();
#endif // RTTI_ENABLE_DRAWING
		return false;
	}

	bool TypeDrawerSameLine::DrawType(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		ImGui::SameLine();
#endif // RTTI_ENABLE_DRAWING
		return false;
	}

	bool TypeDrawerInfoBox::DrawType(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
		ImGui::BeginChild(IMGUI_ID(_message), { -50, 50 }, true);
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
		ImGui::Text(_message);
		ImGui::EndChild();
#endif // RTTI_ENABLE_DRAWING
		
		return false;
	}

	bool TypeDrawerButton::DrawType(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
		if (ImGui::Button(_label))
		{
			FunctionCallingContext functionContext;
			functionContext.object = context.objectStart;
			_function->Invoke(functionContext);
		}
#endif // RTTI_ENABLE_DRAWING
        
		return false;
	}

}
