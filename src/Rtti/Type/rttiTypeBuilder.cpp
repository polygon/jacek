#include "rttiTypeBuilder.h"
#include "../Drawing/rttiTypeDrawer.h"

namespace rtti
{

	bool rtti::DrawByType(const rtti::TypeBase* type, void* objToDraw, const char* fieldName, const char* imGuiId, int initialOffset)
	{
		YK_ASSERT(type);
		YK_ASSERT(objToDraw);

		TypeDrawingContext context(objToDraw, imGuiId);
		context.offset = initialOffset;
		context.fieldName = fieldName;
		context.type = type;

		return type->GetTypeDrawer()->Draw(context);
	}
}
