#pragma once
#include "rttiTType.h"

#define RTTI_DESCRIPTOR_NAME _typeDescriptor
#define RTTI_BUILDER_NAME _typeBuilder
#define RTTI_VARRIABLE (*((Type*)context.objectStart))


namespace rtti
{
	class TypeBuilder
	{
	public:
		TypeBuilder(TypeBase* typeDescriptor, std::function<void(TypeBase* typeDescriptor)> initializationFunction)
		{
			initializationFunction(typeDescriptor);
		}
	};

	template<typename Type>
	void Serialize(YAML::Emitter& emitter, const Type& objToSerialize)
	{
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->Serialize(emitter, serializationContext);
	}

	template<typename Type>
	void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext)
	{
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->Serialize(emitter, serializationContext);
	}

	template<typename Type>
	void Deserialize(const YAML::Node& node, Type& objToSerialize)
	{
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->Deserialize(node, serializationContext);
	}

	template<typename Type>
	void Deserialize(const YAML::Node& node, SerializationContext serializationContext)
	{
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->Deserialize(node, serializationContext);
	}

	/// \brief Serializes type value to a given file path
	/// 
	/// Deserialized type has to be registered in the reflection system 
	/// @see RTTI_DEFINE macro family)
	/// To specify serialization behaviour set TypeSerializer for a type / property
	template<typename Type>
	void SerializeToFile(const Path& filePath, const Type& objToSerialize)
	{
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->SerializeToFile(filePath, serializationContext);
	}

	/// \brief Deserializes type value from a given file path
	/// 
	/// Deserialized type has to be registered in the reflection system 
	/// @see RTTI_DEFINE macro family)
	/// To specify serialization behaviour set TypeSerializer for a type / property
	template<typename Type>
	void DeserializeFromFile(const Path& filePath, Type& objToDeserialize)
	{
		rtti::SerializationContext serializationContext(&objToDeserialize);
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->DeserializeFromFile(filePath, serializationContext);
	}

	/// \brief Serializes type value from a given string
	/// 
	/// Serialized type has to be registered in the reflection system 
	/// @see RTTI_DEFINE macro family)
	/// To specify serialization behaviour set TypeSerializer for a type / property
	/// 
	/// @param s place to put serialized object in yaml format
	template<typename Type>
	void SerializeToString(String& s, const Type& objToSerialize)
	{
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->SerializeToString(s, serializationContext);
	}

	/// \brief Deserializes type value from a given string
	/// 
	/// Deserialized type has to be registered in the reflection system 
	/// @see RTTI_DEFINE macro family)
	/// To specify serialization behaviour set TypeSerializer for a type / property
	template<typename Type>
	void DeserializeFromString(const std::string_view& yamlSource, Type& objToDeserialize)
	{
		rtti::SerializationContext serializationContext(&objToDeserialize);
		YK_CHECK(rtti::GetTypeDescriptor<Type>())->DeserializeFromString(yamlSource, serializationContext);
	}

	bool DrawByType(const TypeBase* type, void* objToDraw, const char* fieldName = "", const char* imGuiId = "", int initialOffset = 0);

	/// \brief Deserializes type value from a given string
	/// 
	/// Deserialized type has to be registered in the reflection system 
	/// @see RTTI_DEFINE macro family)
	template<typename Type>
	void Draw(Type& objToDraw, const char* fieldName = "", const char* imGuiId = "", int initialOffset = 0)
	{
		const rtti::TypeBase* type;
		if constexpr (std::is_class_v<Type>)
		{
			if constexpr (std::is_member_function_pointer<decltype(&Type::GetType)>::value)
			{
				type = objToDraw.GetType();
			}
			else
			{
				type = GetTypeDescriptor<Type>();
			}
		}
		else
		{
			type = GetTypeDescriptor<Type>();
		}

		YK_ASSERT(type);
		DrawByType(type, &objToDraw, fieldName, imGuiId, initialOffset);
	}

	
}


/// /brief Registers a type into the reflection system
/// 
/// inside _Body expression define propertities
/// @warrning do not put this macro inside any namespace
#define RTTI_DEFINE(_Type, _Body)\
template<>\
struct rtti::Internal::TypeDescriptor<_Type>\
{\
	TypeBase* operator()()\
	{\
		using Type = _Type;\
		static TType<std::remove_reference_t<Type>>	RTTI_DESCRIPTOR_NAME(YK_STRINGIFY(_Type));\
		static TypeBuilder	RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](TypeBase* RTTI_DESCRIPTOR_NAME)\
		{\
			RTTI_SERIALIZER(rtti::TypeSerializerNoSerialize);					\
			_Body																\
			RTTI_DESCRIPTOR_NAME->FinalizeInitialization();						\
		});\
		return &RTTI_DESCRIPTOR_NAME;\
	}\
};
