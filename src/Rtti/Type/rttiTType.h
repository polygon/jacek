#pragma once
#include "rttiTypeBase.h"
#include "../Serialization/StringConversion.h"

namespace rtti
{
	/// /brief type template for ease of creation of nonabstract type objects
	template<typename Type>
	class TType : public TypeBase
	{
	public:
		TType(const char* name)
			: TypeBase(name, sizeof(Type))
		{
		}

	protected:
		virtual void* CreateNew() const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				return new Type;
			else
				return nullptr;
		}
		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				return new(memory) Type{};
			else
				return nullptr;
		}
		virtual void Destroy(void* object) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				delete ((Type*)object);
		}
		virtual void DestroyInPlace(void* object) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				((Type*)object)->~Type();
		}
		virtual void* DuplicateNew(const void* original) const override
		{
			if constexpr (std::is_abstract<Type>::value == false)
			{
				if constexpr (std::is_copy_constructible<Type>::value)
				{
					return new Type(*((const Type*)original));// Call copy constructor
				}
			}

			return nullptr;
		}


		virtual void* MoveDuplicateNew(const void* original) const override
		{
			if constexpr (std::is_abstract<Type>::value == false)
			{
				if constexpr (std::is_move_constructible<Type>::value)
				{
					return new Type(std::move(*((const Type*)original)));// Call move constructor
				}
			}

			return nullptr;
		}

		virtual bool SetFromString(void* ptr, const char* valueString) const override
		{
			return rtti::SetFromString<Type>((Type*)ptr, valueString);
		};
	};

	/// \brief specialization for void, cause we cannot instantiate _it
	template<>
	class TType<void> : public TypeBase
	{
	public:
		TType(const char* name)
			: TypeBase(name, 0)
		{
		}

		virtual void* CreateNew() const override
		{
			return nullptr;
		}
		virtual void* CreateNewInPlace(void* memory) const override
		{
			return nullptr;
		}
		virtual void  Destroy(void* object) const override
		{
		}
		virtual void DestroyInPlace(void* object) const override
		{
		}

	protected:
		virtual void* DuplicateNew(const void* original) const override
		{
			return nullptr;
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			return nullptr;
		}
	};
}
