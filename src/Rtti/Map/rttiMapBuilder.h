#pragma once
#include "rttiMapType.h"
#include "../Type/rttiTypeBuilder.h"

namespace rtti { namespace Internal {
	template<typename KeyType, typename ElementType>
	struct TypeDescriptor<TTreeMap<KeyType, ElementType>>
	{
		TypeBase* operator()()
		{
			// TODO correct native name computation
			static TMapType<KeyType, ElementType> RTTI_DESCRIPTOR_NAME(BUILD_TEMPLATED_NAME(TTreeMap, KeyType, ElementType));
			return &RTTI_DESCRIPTOR_NAME;
		}
	};
}}
