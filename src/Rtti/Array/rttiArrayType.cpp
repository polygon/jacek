#include "rttiArrayType.h"

//#include "../../Editor/BasicTypeDrawers.h"
#include "../Class/rttiProperty.h"

#include "../Class/rttiClassType.h"

namespace rtti
{
	enum class EDelayedCommand
	{
		EDuplicate,
		ERemove,
		ESwap,
	};
	struct DelayedCommandData
	{
		EDelayedCommand commantType;
		size_t id1;
		size_t id2;
	};
	bool ArrayType::DrawHeader(TypeDrawingContext& context) const
	{
        bool anyChanged = false;
#ifdef RTTI_ENABLE_DRAWING
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow
			//| ImGuiTreeNodeFlags_AllowItemOverlap
			| ImGuiTreeNodeFlags_DefaultOpen
			;

		float offset = ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP;
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
		ImGui::SetNextItemWidth(10);
		const char* propertyName = context.GetDisplayName();
		bool opened = ImGui::TreeNodeEx(IMGUI_ID(propertyName), flags);
		return opened;
	}
	void ArrayType::EndHeader(TypeDrawingContext& context) const
	{
		ImGui::TreePop();
	}
	bool ArrayType::DrawType(TypeDrawingContext& context) const
	{
		ImGui::SameLine();
		ImGui::SetNextItemWidth(200);
		
		
		int size = (int)GetArraySize(context.objectStart);
		
		ArrayTraits traits = GetArrayTraits();
		
		if (traits.resizable == false)
		{
			ImGui::BeginDisabled();
		}
		bool resized = ImGui::InputInt(IMGUI_ID("size"), &size);
		
		if (traits.resizable == false)
		{
			ImGui::EndDisabled();
		}



		std::vector<DelayedCommandData> delayedCommands;
		delayedCommands.clear();

		bool openedThisFrame = false;

		
		size_t arraySize = GetArraySize(context.objectStart);

		for (size_t i = 0; i < arraySize; ++i)
		{
			void* arrayElemetAddress = GetArrayElementAdress(i, context.objectStart);
			
			TypeDrawingContext elementContext{ context };
			elementContext.offset = context.offset + 1;
			elementContext.objectStart = arrayElemetAddress;
			elementContext.property = nullptr;
			elementContext.imGuiId = IMGUI_ID("");
			elementContext.type = GetArrayElementType();
			
			TempString fieldName = TempString("[") + i + "]";
			elementContext.fieldName = fieldName.GetBuffer();

			ImGuiPushID id((int)i);

			auto afterLabelCall = [&delayedCommands, i, &fieldName, traits]() {

				const char* popup = "blablabla";
				const char* dragReorderCode = "ArrayElement";

				if (ImGui::IsItemClicked(1))
				{
					ImGui::OpenPopup(popup);
				}

				if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID))
				{
					ImGui::LabelText(fieldName.GetBuffer(), "");

					ImGui::SetDragDropPayload(dragReorderCode, &i, sizeof(i));
					ImGui::EndDragDropSource();
				}

				// swap elements // TODO insert in place instead?
				if (ImGui::BeginDragDropTarget())
				{
					if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(dragReorderCode))
					{
						size_t id1 = *((size_t*)payload->Data);
						delayedCommands.push_back({ EDelayedCommand::ESwap, i, id1 });
					}
					ImGui::EndDragDropTarget();
				}


				if (ImGui::BeginPopup(popup))
				{

					if (traits.canDeleteElements && ImGui::MenuItem(IMGUI_ID("Delete")))
					{
						delayedCommands.push_back({ EDelayedCommand::ERemove, i });
					}

					if (traits.canDuplicateElements && ImGui::MenuItem(IMGUI_ID("Duplicate")))
					{
						delayedCommands.push_back({ EDelayedCommand::EDuplicate, i });
					}
					ImGui::EndPopup();
				}
			};
			anyChanged |= GetArrayElementType()->GetTypeDrawer()->Draw(elementContext, afterLabelCall);
		}

		for (auto& _it : delayedCommands)
		{
			switch (_it.commantType)
			{
			case EDelayedCommand::EDuplicate:
				DuplicateElement(_it.id1, context.objectStart);
				break;

			case EDelayedCommand::ERemove:
				RemoveElement(_it.id1, context.objectStart);
				break;

			case EDelayedCommand::ESwap:
				Swap(_it.id1, _it.id2, context.objectStart);
				break;
			}
		}

		if (resized && size >= 0)
		{
			Resize(size, context.objectStart);
			anyChanged |= true;
		}

#endif // RTTI_ENABLED_DRAWING

		return anyChanged;
	}

	void ArrayType::Serialize(YAML::Emitter& emitter, SerializationContext context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		emitter << YAML::Value << YAML::BeginSeq;

		size_t arraySize = GetArraySize(context.objectStart);

		for (size_t i = 0; i < arraySize; ++i)
		{
			SerializationContext elementContext{ context };
			elementContext.objectStart = GetArrayElementAdress(i, context.objectStart);

			GetArrayElementType()->GetTypeSerializer()->Serialize(emitter, elementContext);
		}

		emitter << YAML::EndSeq;

#endif // RTTI_ENABLE_SERIALIZATION
	}

	void ArrayType::Deserialize(const YAML::Node& node, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YK_ASSERT(node);
		YK_ASSERT(node.IsSequence());

		ClearArray(serializationContext.objectStart);

		for (auto _it = node.begin(); _it != node.end(); ++_it)
		{
			const YAML::Node& nodeSampler = *_it;
			RE_ASSERT(nodeSampler);


			SerializationContext elementContext{ serializationContext };
			elementContext.objectStart = AddArrayElement(elementContext.objectStart);

			GetArrayElementType()->GetTypeSerializer()->Deserialize(nodeSampler, elementContext);
		}

#endif // RTTI_ENABLE_SERIALIZATION
	}
}
