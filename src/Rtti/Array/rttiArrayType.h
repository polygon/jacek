#pragma once
#include "../Type/rttiTypeBase.h"
#include "../Drawing/rttiTypeDrawer.h"
#include "../Serialization/rttiSerialization.h"
//#include <algorithm>

namespace rtti
{

	struct ArrayTraits
	{
		bool resizable;
		bool canDeleteElements;
		bool canDuplicateElements;
	};
	class ArrayType : public TypeBase, public TypeDrawerBase, public ITypeSerializerBase
	{
	public:

		ArrayType(const char* name, uint16 size)
			: TypeBase(name, size)
		{
			SetTypeDrawer(this);
			SetTypeSerializer(this);
		}

		virtual const TypeBase* GetArrayElementType() const = 0;

		virtual void* GetArrayElementAdress(size_t id, void* arrayStart) const = 0;

		virtual void* AddArrayElement(void* arrayStart) const = 0;

		virtual void ClearArray(void* arrayStart) const = 0;

		virtual void Resize(size_t size, void* arrayStart) const = 0;

		virtual void Swap(size_t id1, size_t id2, void* arrayStart) const = 0;

		virtual void RemoveElement(size_t id, void* arrayStart) const = 0;

		virtual void DuplicateElement(size_t id1, void* arrayStart) const = 0;

		virtual size_t GetArraySize(void* arrayStart) const = 0;

		virtual ArrayTraits GetArrayTraits() const = 0;

	public: // Drawing
		virtual bool DrawHeader(TypeDrawingContext& context) const override;
		virtual void EndHeader(TypeDrawingContext& context) const override;

		virtual bool DrawType(TypeDrawingContext& context) const override;

	public: // Serializing
		virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override;
		virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override;

	
	};


	template<typename ElementType>
	class TDynArrayType : public ArrayType
	{
	public:
		using ArrayType = std::vector<ElementType>;
		TDynArrayType(const char* name)
			: rtti::ArrayType(name, sizeof(ArrayType))
		{
		}


		virtual ArrayTraits GetArrayTraits() const override
		{
			ArrayTraits traits;
			traits.canDeleteElements = true;
			traits.canDuplicateElements = true;
			traits.resizable = true;

			return traits;
		}


		const TypeBase* GetArrayElementType() const { return GetTypeDescriptor<ElementType>(); }

	protected:
		void* GetArrayElementAdress(size_t id, void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return nullptr;
			else  if constexpr (std::is_same<bool, ElementType>::value)
				return nullptr;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				return array->data() + id;
			}
		}

		void* AddArrayElement(void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return nullptr;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				array->push_back(ElementType{});
				auto& back = array->back();
				return &back;
			}
		}

		void ClearArray(void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				array->clear();
			}
		}

		void Resize(size_t size, void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				array->resize(size);
			}
		}

		void Swap(size_t id1, size_t id2, void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value || std::is_copy_assignable_v<ElementType> == false)
				return;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;

				auto it1 = array->begin() + id1;
				auto it2 = array->begin() + id2;

				auto temp = *it1;
				*it1 = *it2;
				*it2 = temp;
			}
		}

		void RemoveElement(size_t id, void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				array->erase(array->begin() + id);
			}
		}

		void DuplicateElement(size_t id1, void* arrayStart) const
		{
			if constexpr (std::is_copy_constructible<ElementType>::value)
			{
				ArrayType* array = (ArrayType*)arrayStart;
				array->emplace(array->begin() + 1 + id1, (*array)[id1] );
			}
		}

		size_t GetArraySize(void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return 0;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				return array->size();
			}
		}

	protected:

		virtual void* CreateNew() const override
		{
			if constexpr (std::is_abstract<ElementType>::value == false && std::is_copy_constructible<ElementType>::value)
				return new ArrayType();
			else
				return nullptr;
		}

		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<ElementType>::value && std::is_abstract<ElementType>::value == false)
				return new(memory) ArrayType{};
			else
				return nullptr;
		}

		virtual void  Destroy(void* object) const override
		{
			if constexpr (std::is_constructible<ElementType>::value && std::is_abstract<ElementType>::value == false)
				delete ((ArrayType*)object);
		}

		virtual void DestroyInPlace(void* object) const override
		{
			if constexpr (std::is_default_constructible<ArrayType>::value && std::is_abstract<ArrayType>::value == false)
				((ArrayType*)object)->~ArrayType();
		}

		virtual void* DuplicateNew(const void* original) const override
		{
			if constexpr (std::is_copy_constructible<ElementType>::value)
			{
				return new ArrayType(*((const ArrayType*)original));
			}

			return nullptr;
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			
			if constexpr (std::is_trivially_move_constructible<ElementType>::value)
			{
				return nullptr;
			}
			else if constexpr (std::is_move_constructible<ArrayType>::value && std::is_move_constructible<ElementType>::value)
			{
				auto ptr = new ArrayType();
				*ptr = std::move(*((ArrayType*)original));
				return ptr;
			}

			return nullptr;
		}
	};


	template<typename ElementType, uint32 size>
	class TStaticArrayType : public ArrayType
	{
	public:
		using ArrayType = std::array<ElementType, size>;
		TStaticArrayType(const char* name)
			: rtti::ArrayType(name, sizeof(ArrayType))
		{
		}


		virtual ArrayTraits GetArrayTraits() const override
		{
			ArrayTraits traits;
			traits.canDeleteElements = false;
			traits.canDuplicateElements = false;
			traits.resizable = false;

			return traits;
		}

		const TypeBase* GetArrayElementType() const { return GetTypeDescriptor<ElementType>(); }

	protected:
		void* GetArrayElementAdress(size_t id, void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value)
				return nullptr;
			else  if constexpr (std::is_same<bool, ElementType>::value)
				return nullptr;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;
				return array->data() + id;
			}
		}

		void* AddArrayElement(void* arrayStart) const
		{
			//if constexpr (std::is_abstract<ElementType>::value)
				return nullptr;
			//else
			//{
			//	ArrayType* array = (ArrayType*)arrayStart;
			//	array->push_back(ElementType{});
			//	auto& back = array->back();
			//	return &back;
			//}
		}

		void ClearArray(void* arrayStart) const
		{
			//if constexpr (std::is_abstract<ElementType>::value)
				return;
			//else
			//{
			//	ArrayType* array = (ArrayType*)arrayStart;
			//	array->clear();
			//}
		}

		void Resize(size_t size, void* arrayStart) const
		{
			//if constexpr (std::is_abstract<ElementType>::value)
				return;
			//else
			//{
			//	ArrayType* array = (ArrayType*)arrayStart;
			//	array->resize(size);
			//}
		}

		void Swap(size_t id1, size_t id2, void* arrayStart) const
		{
			if constexpr (std::is_abstract<ElementType>::value || std::is_copy_assignable_v<ElementType> == false)
				return;
			else
			{
				ArrayType* array = (ArrayType*)arrayStart;

				auto it1 = array->begin() + id1;
				auto it2 = array->begin() + id2;

				auto temp = *it1;
				*it1 = *it2;
				*it2 = temp;
			}
		}

		void RemoveElement(size_t id, void* arrayStart) const
		{
			//if constexpr (std::is_abstract<ElementType>::value)
				return;
			//else
			//{
			//	ArrayType* array = (ArrayType*)arrayStart;
			//	array->erase(array->begin() + id);
			//}
		}

		void DuplicateElement(size_t id1, void* arrayStart) const
		{
			//if constexpr (std::is_copy_constructible<ElementType>::value)
			//{
			//	ArrayType* array = (ArrayType*)arrayStart;
			//	array->push_back((*array)[id1]);
			//}
		}

		size_t GetArraySize(void* arrayStart) const
		{
			return size;
		}

	protected:

		virtual void* CreateNew() const override
		{
			if constexpr (std::is_abstract<ElementType>::value == false && std::is_copy_constructible<ElementType>::value)
				return new ArrayType();
			else
				return nullptr;
		}

		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<ElementType>::value && std::is_abstract<ElementType>::value == false)
				return new(memory) ArrayType{};
			else
				return nullptr;
		}

		virtual void  Destroy(void* object) const override
		{
			if constexpr (std::is_constructible<ElementType>::value && std::is_abstract<ElementType>::value == false)
				delete ((ArrayType*)object);
		}

		virtual void DestroyInPlace(void* object) const override
		{
			if constexpr (std::is_default_constructible<ArrayType>::value && std::is_abstract<ArrayType>::value == false)
				((ArrayType*)object)->~ArrayType();
		}

		virtual void* DuplicateNew(const void* original) const override
		{
			if constexpr (std::is_copy_constructible<ElementType>::value)
			{
				return new ArrayType(*((const ArrayType*)original));
			}

			return nullptr;
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{

			if constexpr (std::is_trivially_move_constructible<ElementType>::value)
			{
				return nullptr;
			}
			else if constexpr (std::is_move_constructible<ArrayType>::value && std::is_move_constructible<ElementType>::value)
			{
				auto ptr = new ArrayType();
				*ptr = std::move(*((ArrayType*)original));
				return ptr;
			}

			return nullptr;
		}
	};

	// TODO StaticArrayType
	// TODO TempArrayType - we should we serialize or display temp array? 
	// TODO Slice

	// TODO add param to Array - if the array is extendable
	// TODO make sure adding element in ui checks if it succeded
	//			- maybe a boolean function checking if we can add an element right now?
	//			- if not grey out buttons

	// TODO nested array - ensure will work
	// TODO rework properties - their name should be displayed by class / property, not by type

}

namespace rtti {
	namespace Internal {
		

		template<typename ElementType>
		struct TypeDescriptor<TDynArray<ElementType>>
		{
			TypeBase* operator()()
			{
				static TDynArrayType<ElementType> RTTI_DESCRIPTOR_NAME(YK_STRINGIFY(TDynArray<ElementType>));
				return &RTTI_DESCRIPTOR_NAME;
			}
		};

		template<typename ElementType, uint32 size>
		struct TypeDescriptor<TStaticArray<ElementType, size>>
		{
			TypeBase* operator()()
			{
				static TStaticArrayType<ElementType, size> RTTI_DESCRIPTOR_NAME(BUILD_TEMPLATED_NAME(TStaticArrayType, ElementType, size));
				return &RTTI_DESCRIPTOR_NAME;
			}
		};
	}
}
