#pragma once
#include "rttiInterface.h"
#include "../Type/rttiTypeBuilder.h"

namespace rtti
{
	class InterfaceBuilder
	{
	public:
		InterfaceBuilder(InterfaceType* typeDescriptor, std::function<void(InterfaceType*)> initializer)
		{
			initializer(typeDescriptor);
		}
	};
}

#define RTTI_DECLARE_INTERFACE(_Interface)\
	private:\
		static rtti::InterfaceBuilder RTTI_BUILDER_NAME;\
	private:\
		friend class rtti::InterfaceBuilder;\
		friend struct rtti::Internal::TypeDescriptor<_Interface>;\
	public:\
		EXPAND(NO_DISCARD) const rtti::InterfaceType* GetType() const { return GetStaticType(); }\
		static EXPAND(NO_DISCARD) rtti::InterfaceType* GetStaticType();\

#define RTTI_DEFINE_INTERFACE(_Interface, _BODY)																			\
rtti::InterfaceBuilder _Interface::RTTI_BUILDER_NAME(_Interface::GetStaticType(), [](rtti::InterfaceType*) {});					\
rtti::InterfaceType* _Interface::GetStaticType()																			\
{																												\
	static rtti::TInterface<std::remove_reference_t<_Interface>> RTTI_DESCRIPTOR_NAME(YK_STRINGIFY(_Interface));									\
	static rtti::InterfaceBuilder RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](rtti::InterfaceType* RTTI_DESCRIPTOR_NAME)\
	{																											\
		using Type = _Interface;																						\
		_BODY																									\
	});																											\
	return &RTTI_DESCRIPTOR_NAME;																				\
}																												\
namespace rtti { namespace Internal \
{\
	template<> inline void ___FakeCallFunction<_Interface>() { std::cout << _Interface::GetStaticType()->GetDisplayName() << "\n"; } \
}}
