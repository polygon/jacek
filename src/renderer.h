#pragma once

#include <span>

class Renderer {
public:
    virtual void Init(std::span<float> geometry) = 0;
    virtual void Run() = 0;
    virtual void Cleanup() = 0;
};
