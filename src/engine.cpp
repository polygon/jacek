#include "engine.h"
#if defined (DX12) && DX12 == 1 && (defined (WIN32) || defined (_WIN32))
	#include "dx12_engine.h"
#endif
#include "vk_engine.h"
#include <filesystem>

Engine::Engine() : renderer(nullptr)
{
#if defined (DX12) && DX12 == 1 && (defined (WIN32) || defined (_WIN32))
	renderer = new Dx12Engine();
#else
	renderer = new VulkanEngine();
#endif
}

Engine::~Engine()
{
	delete renderer;
}

void Engine::Init(std::span<float> geometry)
{
	renderer->Init(geometry);
}

void Engine::Run()
{

	renderer->Run();
}

void Engine::Cleanup()
{
	renderer->Cleanup();
}
