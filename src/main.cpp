#include "engine.h"
#include <span>

int main(int argc, char *argv[]) {
    float triangle_vertices[] = {
            -0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f,
            0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f};

    float arrow_vertices[] = {
            0.0f, 0.5f, 0.0f,
            0.5f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f,
            -0.5f, 0.0f, 0.0f,
            0.0f, 0.5f, 0.0f,
            0.0f, 0.0f, 0.0f,
            -0.25f, 0.0f, 0.0f,
            0.25f, 0.0f, 0.0f,
            -0.25f, -0.5f, 0.0f,
            0.25f, 0.0f, 0.0f,
            0.25f, -0.5f, 0.0f,
            -0.25f, -0.5f, 0.0f};

    std::span<float> triangle(triangle_vertices);
    std::span<float> arrow(arrow_vertices);

    Engine engine;
    engine.Init(arrow);
    engine.Run();
    engine.Cleanup();

    return 0;
}
