// vulkan_guide.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#include "vk_types.h"
#include <deque>
#include <functional>
#include <vector>

#include "renderer.h"

class PipelineBuilder {
public:
    std::vector<VkPipelineShaderStageCreateInfo> _shaderStages;
    VkPipelineVertexInputStateCreateInfo _vertexInputInfo;
    VkPipelineInputAssemblyStateCreateInfo _inputAssembly;
    VkViewport _viewport;
    VkRect2D _scissor;
    VkPipelineRasterizationStateCreateInfo _rasterizer;
    VkPipelineColorBlendAttachmentState _colorBlendAttachment;
    VkPipelineMultisampleStateCreateInfo _multisampling;
    VkPipelineLayout _pipelineLayout;

    VkPipeline build_pipeline(VkDevice device, VkRenderPass pass);
};


struct DeletionQueue {
    std::deque<std::function<void()>> deletors;

    void push_function(std::function<void()> &&function) {
        deletors.push_back(function);
    }

    void flush() {
        // reverse iterate the deletion queue to execute all the functions
        for (auto it = deletors.rbegin(); it != deletors.rend(); it++) {
            (*it)();//call functors
        }

        deletors.clear();
    }
};


class VulkanEngine : public Renderer {
public:
    bool _isInitialized{false};
    int _frameNumber{0};
    int _selectedShader{0};

    VkExtent2D _windowExtent{1700, 900};

    struct SDL_Window *_window{nullptr};

    bool debug_utils_extension_enabled_ = false;
    VkInstance _instance;
    VkDebugUtilsMessengerEXT _debug_messenger;
    VkPhysicalDevice _chosenGPU;
    VkDevice _device;
    VmaAllocator allocator_ = VK_NULL_HANDLE;

    VkSemaphore _presentSemaphore, _renderSemaphore;
    VkFence _renderFence;

    VkQueue _graphicsQueue;
    uint32_t _graphicsQueueFamily;

    VkCommandPool _commandPool;
    VkCommandBuffer _mainCommandBuffer;

    VkRenderPass _renderPass;

    VkSurfaceKHR _surface;
    VkSwapchainKHR _swapchain;
    VkFormat _swachainImageFormat;

    std::vector<VkFramebuffer> _framebuffers;
    std::vector<VkImage> _swapchainImages;
    std::vector<VkImageView> _swapchainImageViews;

    VkPipelineLayout _trianglePipelineLayout;

    VkPipeline _trianglePipeline;
    VkPipeline _redTrianglePipeline;

    DeletionQueue _mainDeletionQueue;

    //initializes everything in the engine
    void Init(std::span<float> geometry) override;

    //shuts down the engine
    void Cleanup() override;

    //draw loop
    void draw();

    //run main loop
    void Run() override;

private:
    void init_vulkan();
    void InitAllocator();

    void init_swapchain();

    void init_default_renderpass();

    void init_framebuffers();

    void init_commands();

    void init_sync_structures();

    void init_pipelines();

    //loads a shader module from a spir-v file. Returns false if it errors
    bool load_shader_module(const char *filePath, VkShaderModule *outShaderModule);

    void SetObjectName(VkObjectType type, uint64_t handle, const char *name);
    void SetObjectName(VkPipeline handle, const char *name) {
        SetObjectName(VK_OBJECT_TYPE_PIPELINE, reinterpret_cast<uint64_t>(handle), name);
    }
    void SetObjectName(VkBuffer handle, const char *name) {
        SetObjectName(VK_OBJECT_TYPE_BUFFER, reinterpret_cast<uint64_t>(handle), name);
    }
    void SetObjectName(VkImage handle, const char *name) {
        SetObjectName(VK_OBJECT_TYPE_IMAGE, reinterpret_cast<uint64_t>(handle), name);
    }
    // Add overloads here for more Vulkan types as needed, for convenience...

    void BeginLabel(VkCommandBuffer cmd_buf, const char *name);
    void EndLabel(VkCommandBuffer cmd_buf);

    class LabelScope {
    public:
        LabelScope(VulkanEngine &engine, VkCommandBuffer cmd_buf, const char *name)
            : engine_{engine},
              cmd_buf_{cmd_buf} {
            engine.BeginLabel(cmd_buf, name);
        }
        ~LabelScope() {
            engine_.EndLabel(cmd_buf_);
        }

    private:
        VulkanEngine &engine_;
        VkCommandBuffer cmd_buf_;
    };
};
