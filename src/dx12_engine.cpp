#include "dx12_engine.h"
#include "d3dx12.h"

#include <SDL_syswm.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>

#include "ResourcesPaths.h"

TResolution<1700, 900> screenResolution;

void Dx12Engine::Init(std::span<float> geometry)
{
	num_of_vertices_ = UINT(geometry.size() / 3);
	Log("Requested to draw", num_of_vertices_, "vertices");
	InitializeSdl();
	CreateDxgiAndEnumAdapters();
	CreateDxDevice();
	CreateCommandQueue();
	CreateSwapChain();
	CreateCommandAllocator();
	CreateCommandList();
	CreateDescriptorHeap();
	CreateFrameResources();
	CreateFence();
	CreateRootSignature();
	CreatePipelineState();
	CreateVertexBuffer(geometry);
}

void Dx12Engine::Cleanup()
{
	SafeRelease(pipeline_state_);
	SafeRelease(root_signature_);
	SafeRelease(fence_);
	for (UINT i = 0; i < 2; i++)
	{
		SafeRelease(render_targets_.array[i]);
	}
	SafeRelease(rtv_descriptor_heap_);
	delete dx12_command_list_;
	delete dx12_command_allocator_;
	SafeRelease(dxgi_swap_chain_);
	SafeRelease(command_queue_);
	SafeRelease(device_);
	SafeRelease(dxgi_factory_);
	rtv_descriptor_size_ = { 0 };
	SDL_DestroyWindow(window_);
}

void Dx12Engine::Draw()
{
	RecordCommandList(dx12_command_list_->GetRaw());
	ExecuteCommandList();
	WaitForPreviousFrame();
}

void Dx12Engine::ExecuteCommandList()
{
	ID3D12CommandList* ppCommandList[] = { dx12_command_list_->GetRaw() };
	command_queue_->ExecuteCommandLists(_countof(ppCommandList), ppCommandList);
	dxgi_swap_chain_->Present(1, 0);
}

void Dx12Engine::RecordCommandList(ID3D12GraphicsCommandList* command_list_)
{
	// Reclaim Command List memory
	//command_allocator_->Reset();
	dx12_command_allocator_->Reset();
	command_list_->Reset(dx12_command_allocator_->GetRaw(), pipeline_state_);

	// Set the Root Signature
	command_list_->SetGraphicsRootSignature(root_signature_);

	// Setup Viewport and Scissors Rectangle
	command_list_->RSSetViewports(1, &viewport_);
	command_list_->RSSetScissorRects(1, &scissor_rect_);

	// First, we have to transform our Render Target to appropriate state
	auto barrier = CD3DX12_RESOURCE_BARRIER::Transition(render_targets_.array[frame_index_], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
	command_list_->ResourceBarrier(1, &barrier);

	// Let's set and clear Render Target
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(rtv_descriptor_heap_->GetCPUDescriptorHandleForHeapStart(), frame_index_, rtv_descriptor_size_);
	command_list_->OMSetRenderTargets(1, &rtvHandle, FALSE, nullptr);
	const float clearColor[] = { 0.0f, 0.2f, 0.4f, 1.0f };
	command_list_->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	command_list_->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	command_list_->IASetVertexBuffers(0, 1, &vertex_buffer_view_);
	command_list_->DrawInstanced(num_of_vertices_, 1, 0, 0);

	// Now we have to transition our Render Target back to the Present state
	barrier = CD3DX12_RESOURCE_BARRIER::Transition(render_targets_.array[frame_index_], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
	command_list_->ResourceBarrier(1, &barrier);

	command_list_->Close();
}

void Dx12Engine::HandleInput(SDL_Event& event)
{
	if (event.type == SDL_QUIT)
	{
		quit = true;
	}
	else
	{
		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_ESCAPE:
					quit = true;
					break;
				default:
					;
			}
		}
	}
}

void Dx12Engine::Run()
{
	//main loop
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&event))
		{
			HandleInput(event);
		}

		Draw();
	}
}

void Dx12Engine::InitializeSdl()
{
	Log("Initialize SDL...");

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "ERROR: Couldn't initialize SDL: " << SDL_GetError() << "\n";
		exit(1);
	}

	//Create window
	window_ = SDL_CreateWindow("JACEK Engine (dx12)", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenResolution.width, screenResolution.height, SDL_WINDOW_SHOWN);
	if (window_ == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
	}

	// Get Window Handle
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	SDL_GetWindowWMInfo(window_, &wmInfo);
	hwnd = wmInfo.info.win.window;
}

void Dx12Engine::CreateDxgiAndEnumAdapters()
{
	Log("Create DXGI Factory and enumerate adapters...");

	auto result = CreateDXGIFactory2(0, IID_PPV_ARGS(&dxgi_factory_));
	if (FAILED(result))
	{
		exit(1);
	}

	// Enumerate the adapters
	IDXGIAdapter1* temp_adapter;
	UINT index = 0;

	IDXGIFactory6* factory6 = NULL;
	result = dxgi_factory_->QueryInterface(IID_PPV_ARGS(&factory6));
	if(SUCCEEDED(result))
	{
		IDXGIAdapter* adapter0 = NULL;
		while (DXGI_ERROR_NOT_FOUND != factory6->EnumAdapterByGpuPreference(
			index++, DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(&adapter0)))
		{
			DXGI_ADAPTER_DESC adapterDesc;
			adapter0->GetDesc(&adapterDesc);
			std::wcout << "\t" << index << ") " << adapterDesc.Description << ", dedicated VRAM: " << adapterDesc.DedicatedVideoMemory / 1024 / 1024 << " MB\n";
			adapter0->Release();
		}
		// Finally we pick the first adapter
		factory6->EnumAdapterByGpuPreference(0, DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(&adapter0));
		adapter0->QueryInterface(IID_PPV_ARGS(&temp_adapter));
		adapter0->Release();
	}
	else
	{
		while (DXGI_ERROR_NOT_FOUND != dxgi_factory_->EnumAdapters1(index++, &temp_adapter))
		{
			DXGI_ADAPTER_DESC1 adapterDesc;
			temp_adapter->GetDesc1(&adapterDesc);
			std::wcout << "\t" << index << ") " << adapterDesc.Description << ", dedicated VRAM: " << adapterDesc.DedicatedVideoMemory / 1024 / 1024 << " MB\n";
			temp_adapter->Release();
		}
		// Finally we pick the first adapter
		dxgi_factory_->EnumAdapters1(0, &dxgi_adapter_);
	}

	Log("Number of found adapters:", index - 1);
}

void Dx12Engine::CreateDxDevice()
{
	Log("Create DX12 Device...");

#if _DEBUG
	// API level validation
	ID3D12Debug* debugInterface_0;
	D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface_0));
	debugInterface_0->EnableDebugLayer();

	// GPU level validation
	ID3D12Debug1* debugInterface_1;
	debugInterface_0->QueryInterface(IID_PPV_ARGS(&debugInterface_1));
	debugInterface_1->SetEnableGPUBasedValidation(true);
#endif

	auto result = D3D12CreateDevice(dxgi_adapter_, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&device_));
	if (FAILED(result))
	{
		exit(1);
	}
}

void Dx12Engine::CreateCommandQueue()
{
	Log("Create DX12 Command Queue...");

	// Create Command Queue
	D3D12_COMMAND_QUEUE_DESC commandQueueDesc;
	ZeroMemory(&commandQueueDesc, sizeof(commandQueueDesc));

	commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	commandQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	commandQueueDesc.NodeMask = 0;

	auto result = device_->CreateCommandQueue(&commandQueueDesc, IID_PPV_ARGS(&command_queue_));
	if (FAILED(result))
	{
		exit(1);
	}
	command_queue_->SetName(L"MyCommandQueue");
}

void Dx12Engine::CreateSwapChain()
{
	Log("Initialize Swap Chain...");

	// Create Swap Chain
	DXGI_SAMPLE_DESC sampleDesc;
	ZeroMemory(&sampleDesc, sizeof(sampleDesc));
	sampleDesc.Count = 1;
	sampleDesc.Quality = 0;

	DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.Width = 1700;
	swapChainDesc.Height = 900;
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 2;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.SampleDesc = sampleDesc;

	IDXGISwapChain1* tempSwapChain;
	auto result = dxgi_factory_->CreateSwapChainForHwnd(command_queue_, hwnd, &swapChainDesc, nullptr, nullptr, &tempSwapChain);
	if (FAILED(result))
	{
		exit(1);
	}

	tempSwapChain->QueryInterface(IID_PPV_ARGS(&dxgi_swap_chain_));
	tempSwapChain->Release();
}

void Dx12Engine::CreateCommandAllocator()
{
	Log("Create Command Allocator...");

	dx12_command_allocator_ = new Dx12CommandListAllocator(device_);
}

void Dx12Engine::CreateCommandList()
{
	Log("Create Command List...");

	dx12_command_list_ = new Dx12CommandList(device_, dx12_command_allocator_);
}

void Dx12Engine::CreateDescriptorHeap()
{
	Log("Create RTV Descriptor Heap...");

	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
	rtvHeapDesc.NumDescriptors = 2;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	auto result = device_->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtv_descriptor_heap_));
	if (FAILED(result))
	{
		exit(1);
	}

	rtv_descriptor_size_ = device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
}

void Dx12Engine::CreateFrameResources()
{
	Log("Create Frame Resources...");

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(rtv_descriptor_heap_->GetCPUDescriptorHandleForHeapStart());

	for (UINT n = 0; n < 2; n++)
	{
		auto hey = render_targets_.array[n];
		auto result = dxgi_swap_chain_->GetBuffer(n, IID_PPV_ARGS(&render_targets_.array[n]));
		if (FAILED(result))
		{
			exit(1);
		}
		device_->CreateRenderTargetView(render_targets_.array[n], nullptr, rtvHandle);
		std::string rtName = "MainRenderTarget_" + std::to_string(n);
		std::wstring temp = std::wstring(rtName.begin(), rtName.end());
		render_targets_.array[n]->SetName(temp.c_str());
		rtvHandle.Offset(1, rtv_descriptor_size_);
	}

	frame_index_ = dxgi_swap_chain_->GetCurrentBackBufferIndex();

	viewport_ = { 0.0f, 0.0f, static_cast<float>(screenResolution.width), static_cast<float>(screenResolution.height), 0.0f, 1.0f };
	scissor_rect_ = { 0, 0, static_cast<LONG>(screenResolution.width), static_cast<LONG>(screenResolution.height) };
}

void Dx12Engine::WaitForPreviousFrame()
{
	const UINT64 fence = fence_value_;
	command_queue_->Signal(this->fence_, fence);
	fence_value_++;

	if (this->fence_->GetCompletedValue() < fence)
	{
		this->fence_->SetEventOnCompletion(fence, fence_event_);
		WaitForSingleObject(fence_event_, INFINITE);
	}

	frame_index_ = dxgi_swap_chain_->GetCurrentBackBufferIndex();
}

void Dx12Engine::CreateFence()
{
	Log("Creating Fence...");

	device_->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence_));
	fence_value_ = 1;

	fence_event_ = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (fence_event_ == nullptr)
	{
		exit(1);
	}

	WaitForPreviousFrame();
}

void Dx12Engine::CreateRootSignature()
{
	Log("Creating Root Signature...");

	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	rootSignatureDesc.Init(0, nullptr, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ID3DBlob* signature;
	ID3DBlob* error;
	auto result = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error);
	if (FAILED(result))
	{
		exit(1);
	}
	result = device_->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&root_signature_));
	if (FAILED(result))
	{
		exit(1);
	}
}

void Dx12Engine::CreatePipelineState()
{
	Log("Create Pipeline State...");

	ID3DBlob* vertexShader = nullptr;
	ID3DBlob* pixelShader = nullptr;
	ID3DBlob* errorBlob = nullptr;

#if defined(_DEBUG)
	// Enable better shader debugging with the graphics debugging tools.
	UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	UINT compileFlags = 0;
#endif

	auto result = D3DCompileFromFile(L"shaders\\dx12\\shaders.hlsl", nullptr, nullptr, "VSMain", "vs_5_0", compileFlags, 0, &vertexShader, &errorBlob);
	if (FAILED(result))
	{
		if (errorBlob)
		{
			Log((char*)errorBlob->GetBufferPointer());
			OutputDebugStringA((char*)errorBlob->GetBufferPointer());
		}
		exit(1);
	}
	result = D3DCompileFromFile(L"shaders\\dx12\\shaders.hlsl", nullptr, nullptr, "PSMain", "ps_5_0", compileFlags, 0, &pixelShader, &errorBlob);
	if (FAILED(result))
	{
		if (errorBlob)
		{
			Log((char*)errorBlob->GetBufferPointer());
			OutputDebugStringA((char*)errorBlob->GetBufferPointer());
		}
		exit(1);
	}

	// Define the vertex input layout.
	D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
	{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	// Describe and create the graphics pipeline state object (PSO).
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
	psoDesc.pRootSignature = root_signature_;
	psoDesc.VS = { reinterpret_cast<UINT8*>(vertexShader->GetBufferPointer()), vertexShader->GetBufferSize() };
	psoDesc.PS = { reinterpret_cast<UINT8*>(pixelShader->GetBufferPointer()), pixelShader->GetBufferSize() };
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState.DepthEnable = FALSE;
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	result = device_->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&pipeline_state_));
	if (FAILED(result))
	{
		exit(1);
	}
}

void Dx12Engine::CreateVertexBuffer(std::span<float> geometry)
{
	Log("Create static Vertex Buffer...");

	const UINT vertexSize = 3 * sizeof(float);
	const UINT vertexBufferSize = geometry.size() * sizeof(float);

	// Create heap to fit the data
	CD3DX12_HEAP_PROPERTIES heapProps(D3D12_HEAP_TYPE_UPLOAD);
	auto desc = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);
	auto result = (device_->CreateCommittedResource(&heapProps, D3D12_HEAP_FLAG_NONE, &desc,
		D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&vertex_buffer_)));
	if (FAILED(result))
	{
		exit(1);
	}

	// Copy the data over to the GPU heap
	UINT8* pVertexDataBegin;
	CD3DX12_RANGE readRange(0, 0);
	result = vertex_buffer_->Map(0, &readRange, reinterpret_cast<void**>(&pVertexDataBegin));
	if (FAILED(result))
	{
		exit(1);
	}
	memcpy(pVertexDataBegin, geometry.data(), vertexBufferSize);
	vertex_buffer_->Unmap(0, nullptr);

	// Initialize vertex buffer view
	vertex_buffer_view_.BufferLocation = vertex_buffer_->GetGPUVirtualAddress();
	vertex_buffer_view_.StrideInBytes = vertexSize;
	vertex_buffer_view_.SizeInBytes = vertexBufferSize;
}
