#pragma once
#include <filesystem>

#if defined (WIN32) || defined (_WIN32)
    extern const std::filesystem::path dxShadersPath;
    extern const std::filesystem::path vkShadersPath;
#else ()
    extern const std::filesystem::path vkShadersPath;
#endif ()
