#pragma once

#include <span>

#include "renderer.h"

class Renderer;

class Engine {
public:
    Engine();
    ~Engine();
    void Init(std::span<float> geometry);
    void Run();
    void Cleanup();

private:
    Renderer *renderer;
};
