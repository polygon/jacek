#include "ResourcesPaths.h"

#if defined (WIN32) || defined (_WIN32)
    const std::filesystem::path dxShadersPath = "shaders\\dx12";
    const std::filesystem::path vkShadersPath = "shaders\\vk";
#else ()
    const std::filesystem::path vkShadersPath = "shaders/vk";
#endif ()
