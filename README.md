# JACEK Engine

Jet Another Complicated Engine Kurwa (JACEK Engine) will be an open source 3D game engine developed using C++ and Vulkan.

**License:** MIT

**Docs**
[Wiki & Meeting notes](https://docs.google.com/document/d/1EBT0vbH4EpDC-6x3CO66IugowTLCB8AQV2IPWFrEk4E/edit?usp=sharing)

**Authors:** The engine is developed by [Koło Naukowe Twórców Gier Polygon](https://kntgpolygon.pl/) - game development interest group at [Warsaw University of Technology](https://www.pw.edu.pl/).

# Bulding Process
1. First of all you have to clone the repository. Here's the command line:

`git clone https://gitlab.com/polygon/jacekengine.git`

2. Now you need to bulid a solution. The engine requres CMake 3.8 or higher to run properly. The CMake source tree is the **jacekengine** directory and the build tree should be set to **build** Go into the newly created directory and run one of the two attached BAT files, depending on which rendering backend you want to use.